package br.com.seasolutions.seapoint

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import br.com.seatelecom.auth.data.CHANNEL_ID
import io.realm.*
import io.sentry.android.core.SentryAndroid
import io.sentry.android.core.SentryAndroidOptions
import io.sentry.core.SentryEvent
import io.sentry.core.SentryLevel
import io.sentry.core.SentryOptions
import java.util.*


class App: Application() {

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)


        val config = RealmConfiguration.Builder()
            .name("myrealm.realm")
            .allowWritesOnUiThread(true)
            .schemaVersion(3)
            .migration(MyMigrations())
            .build()

        Realm.setDefaultConfiguration(config)




        SentryAndroid.init(
            this
        ) { options: SentryAndroidOptions ->
            // Add a callback that will be used before the event is sent to Sentry.
            // With this callback, you can modify the event or, when returning null, also discard the event.
            options.beforeSend =
                SentryOptions.BeforeSendCallback { event: SentryEvent, hint: Any? ->
                    if (SentryLevel.DEBUG == event.level)
                        return@BeforeSendCallback null
                    else return@BeforeSendCallback event
                }
        }
        createNotificationChannel()
    }

    private fun createNotificationChannel() {
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){

            val serviceChannel = NotificationChannel(
                CHANNEL_ID,
                "Acompanhamento de Chamado",
                NotificationManager.IMPORTANCE_DEFAULT
            )

            val manager = getSystemService(NotificationManager::class.java)
            manager.createNotificationChannel(serviceChannel)
        }
    }




}

class MyMigrations: RealmMigration{
    override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {

        var version = oldVersion
        val schema = realm.schema
        schema.get("Occupant")!!.addField("companyId", String::class.java, FieldAttribute.REQUIRED)
        schema.get("Material")!!.removeField("amount")
        when(version){
            0L -> {

                version++
                version++
            }
            1L -> version++
            2L ->{

            }
        }
    }

}