package br.com.seasolutions.seapoint.ui.create.point

import android.content.Context
import android.content.DialogInterface
import android.content.DialogInterface.OnMultiChoiceClickListener
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import br.com.seasolutions.core.R
import br.com.seasolutions.core.databinding.ConfigPointUI
import br.com.seasolutions.seapoint.model.Point
import br.com.seasolutions.seapoint.ui.current.CurrentProjectViewModel
import org.osmdroid.config.Configuration
import java.lang.IndexOutOfBoundsException
import java.lang.NullPointerException


class ConfigPointFragment : DialogFragment() {

    private lateinit var binding: ConfigPointUI
    private lateinit var currentProjectViewModel: CurrentProjectViewModel



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        currentProjectViewModel =
            ViewModelProvider(requireActivity()).get(CurrentProjectViewModel::class.java)
        binding = DataBindingUtil
            .inflate(inflater, R.layout.fragment_config_point, container, false)


        val ctx: Context = requireContext()
        Configuration.getInstance().load(ctx, requireActivity().getPreferences(0))

        return binding.root
    }

    override fun onResume() {
        super.onResume()

        binding.conclude.setOnClickListener {
            val point = Point()
            val height = resources.getStringArray(R.array.height)
            var type = resources.getStringArray(R.array.pole_type)
            var effort = resources.getStringArray(R.array.effort)
            point.type = type.get(binding.poleTypeSpinner.selectedItemPosition)
            point.height = Integer.valueOf(height.get(binding.heigthSpinner.selectedItemPosition))
            point.capacity = Integer.valueOf(effort.get(binding.effortSpinner.selectedItemPosition))



            currentProjectViewModel.setDefaultPoint(point)
            findNavController().navigateUp()
        }
        binding.accessories.setOnClickListener { selectAccessories() }
        binding.occupantsButton.setOnClickListener { selectOccupants() }
    }

    fun selectAccessories() {

        val option = ArrayList<String>()
        val selected = ArrayList<Boolean>()
        val index = ArrayList<Int>()
        currentProjectViewModel.accessory.value?.let {
            for (aux in it){
                option.add(aux.name)
                selected.add(aux.isChecked)
            }
        }

        AlertDialog.Builder(requireContext())
            .setMultiChoiceItems(option.toTypedArray(),
                selected.toBooleanArray(),
                OnMultiChoiceClickListener { dialogInterface, which, isChecked ->
                    index.add(which)
                })
            .setTitle("Selecione os acessórios presentes no poste.")
            .setPositiveButton("Confirmar", DialogInterface.OnClickListener { dialog, which ->
                currentProjectViewModel.setAccessory(index)
                currentProjectViewModel.accessory.value?.let {

                    var count = 0
                    for(aux in it){
                        if(aux.isChecked) count++
                    }
                    binding.accessoriesTextView.text = "Acessórios: $count"
                }
            })
            .setNeutralButton("Selecionar todos",
                DialogInterface.OnClickListener{dialog, which ->
                    currentProjectViewModel.selectAllAccessories()
                    val count = selected.size
                    binding.accessoriesTextView.text = "Acessórios: $count"
                })
            .create().show()
    }

    fun selectOccupants() {
        val option = ArrayList<String>()
        val selected = ArrayList<Boolean>()
        val index = ArrayList<Int>()
        //currentProjectViewModel.updateOccupantsofProject()
        try {
            currentProjectViewModel.syncLocalOccupants()
        }catch (e: IndexOutOfBoundsException){

        }catch (e: NullPointerException){

        }

        currentProjectViewModel.occupants.value?.let {
            Log.e("configPointFragmen", "-------------------------")
            for( aux in it){
                option.add(aux.name)
                selected.add(aux.isChecked)

                Log.e("", aux.name)
                Log.e("", aux.isChecked.toString())

            }
            Log.e("configPointFragment", "-------------------------")
        }

//        currentProjectViewModel.occupants.value?.let {
//                for( aux in it){
//                    option.add(aux.name)
//                    selected.add(aux.isChecked)
//
//                    Log.e("occ - 126 - config", aux.name)
//                    Log.e("occ - 126 - config", aux.isChecked.toString())
//                }
//        }
//        Log.e("linha", "------------------------------")



        AlertDialog.Builder(requireContext())
            .setMultiChoiceItems( option.toTypedArray(),
                selected.toBooleanArray(),
                OnMultiChoiceClickListener { dialogInterface, which, isChecked ->
                    index.add(which)
                })
            .setTitle("Selecione os ocupantes presentes no poste.")
            .setNeutralButton("Selecionar todos",
                DialogInterface.OnClickListener{dialog, which ->
                    currentProjectViewModel.selectAllOccupants()
                    val count = selected.size
                    binding.occupantsTextView.text = "Ocupantes: $count"
                })
            .setPositiveButton("Confirmar") { dialog, which ->
                currentProjectViewModel.setOccupant(index)
                currentProjectViewModel.occupants.value?.let {
                    var count = 0
                    for (aux in it) {
                        if (aux.isChecked) count++
                    }
                    binding.occupantsTextView.text = "Ocupantes: $count"
                }
            }
            .create().show()

    }

}