package br.com.seasolutions.seapoint.ui.edit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import br.com.seasolutions.core.R
import br.com.seasolutions.core.databinding.CreateProjectUI
import br.com.seasolutions.seapoint.model.Project
import br.com.seasolutions.seapoint.ui.current.CurrentProjectViewModel
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.login.LoginViewModel

class EditProjectFragment : DialogFragment() {

    private lateinit var binding: CreateProjectUI
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var currentProjectViewModel: CurrentProjectViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_create_project,
            container,
            false
        )
//        title =
        dialog?.setTitle("Novo Projeto")

        loginViewModel = ViewModelProvider(requireActivity())
            .get(LoginViewModel::class.java)
        currentProjectViewModel = ViewModelProvider(requireActivity())[CurrentProjectViewModel::class.java]

        currentProjectViewModel.project.observe(viewLifecycleOwner,  Observer{
            binding.name.setText(it!!.name)
            binding.description.setText(it.description)
            binding.visibility.setSelection(Integer.valueOf(it.visibility))
        })
        binding.save.text = "Salvar alterações"
        binding.save.setOnClickListener {

            var project = Project()

            var user = LoggedInUser().readUser(requireContext())

            project.companyId = user.companies[user.selectedCompany]
            project.operatorId = user.preferredUsername
            if(binding.name.text?.length!=0){
                if(binding.description.text?.length!=0){

                    project.name = binding.name.text.toString()
                    project.description = binding.description.text.toString()
                    project.visibility = binding.visibility.selectedItemPosition.toString()
                    project.status = "toUpdate"
                    currentProjectViewModel.update(project)

                    Toast.makeText(
                        context,
                        "O projeto foi alterado com sucesso!",
                        Toast.LENGTH_SHORT
                    ).show()

                    dismiss()
                } else{
                    binding.description.error = "Campo obrigatório"
                    binding.description.requestFocus()
                }
            } else {
                binding.name.error = "Campo obrigatório"
                binding.name.requestFocus()
            }

        }


        return  binding.root
    }

}