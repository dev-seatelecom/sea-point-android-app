package br.com.seasolutions.seapoint.repo.db.accessory

import androidx.lifecycle.MutableLiveData
import br.com.seasolutions.seapoint.model.Accessory
import io.realm.Realm

class Repository(){

companion object{
    fun get(_accessories: MutableLiveData<ArrayList<Accessory>>) {
        val realm = Realm.getDefaultInstance()
        realm.addChangeListener {
            val result = realm.where(Accessory::class.java).findAll()
            val list = ArrayList<Accessory>()
            list.addAll(result)
            _accessories.postValue(list)
        }
    }
}

}