package br.com.seasolutions.seapoint.ui.create.project

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import br.com.seasolutions.core.R
import br.com.seasolutions.core.databinding.CreateProjectUI
import br.com.seasolutions.seapoint.model.Project
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.login.LoginViewModel

class CreateProjectFragment : DialogFragment() {

    private lateinit var binding: CreateProjectUI
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var currentProjectViewModel: CreateProjectViewModel

    @SuppressLint("ResourceType")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_create_project,
            container,
            false
        )
//        title =
        dialog?.setTitle("Novo Projeto")

        val activity = (requireActivity() as AppCompatActivity)
        var actionBar = activity.supportActionBar
        actionBar?.title = "Adicionar Projeto"
        setHasOptionsMenu(true)

        loginViewModel = ViewModelProvider(requireActivity())
            .get(LoginViewModel::class.java)
        currentProjectViewModel = ViewModelProvider(requireActivity())[CreateProjectViewModel::class.java]
        binding.save.setOnClickListener {

            var project = Project()

            var user = LoggedInUser().readUser(requireContext())
            project.companyId = user!!.companies[user!!.selectedCompany]!!
            project.operatorId = user!!.preferredUsername!!
            if(binding.name.text?.length!=0){
                if(binding.description.text?.length!=0){

                    project.name = binding.name.text.toString()
                    project.description = binding.description.text.toString()

                    project.visibility = binding.visibility.selectedItemPosition.toString()
                    currentProjectViewModel.setProject(project)
                    val action =
                        CreateProjectFragmentDirections.actionCreateProjectFragmentToOccupantFragment()
                    findNavController().navigate(action)
                } else{
                    binding.description.error = "Campo obrigatório"
                    binding.description.requestFocus()
                }
            } else {
                binding.name.error = "Campo obrigatório"
                binding.name.requestFocus()
            }

        }

        binding.cancel.setOnClickListener { dismiss() }

        return  binding.root
    }

}