package br.com.seasolutions.seapoint.model

import com.google.gson.JsonObject
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey
import org.bson.types.ObjectId


open class Accessory(
    var name: String = "",
    var status: String = "toCreate",
    @Ignore var isChecked: Boolean = false,
    @PrimaryKey var _id: String = ObjectId().toString()
): RealmObject(){

    fun getJson():JsonObject{
        val json = JsonObject()

        json.addProperty("name", name)

        return json
    }


}



