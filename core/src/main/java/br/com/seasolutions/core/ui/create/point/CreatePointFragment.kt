package br.com.seasolutions.seapoint.ui.create.point

import android.Manifest
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import br.com.seasolutions.core.R
import br.com.seasolutions.core.databinding.CreatePointUI
import br.com.seasolutions.seapoint.model.Point
import br.com.seasolutions.seapoint.ui.current.CurrentProjectViewModel
import com.google.android.material.snackbar.Snackbar
import io.realm.RealmList
import org.bson.types.ObjectId
import java.io.File
import java.io.IOException
import java.lang.Exception
import java.util.*
import kotlin.jvm.Throws


class CreatePointFragment : DialogFragment() {


    private lateinit var args: CreatePointFragmentArgs
    private lateinit var binding: CreatePointUI
    private lateinit var point: Point
    private lateinit var viewModel: CurrentProjectViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_create_point, container, false)
        viewModel = ViewModelProvider(requireActivity())[CurrentProjectViewModel::class.java]

        binding.seekBarDistance.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                binding.textDistance.setText("Distância: " + progress + "m")
                point.distance = progress
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })

        args = navArgs<CreatePointFragmentArgs>().value

        point = viewModel.point.value!!
        point._id = ObjectId().toString()
        point.createdAt = Calendar.getInstance().time.time
        point.accuracy = args.location.accuracy
        val location = RealmList<Double>()
        location.add(args.location.latitude)
        location.add(args.location.longitude)
        location.add(args.location.altitude)
        point.location = location

        val projectVisibility = viewModel



        binding.create.setOnClickListener {
            if(viewModel.project.value?.visibility.equals("1")){
                take()
            } else {
                alert()
            }
        }

        return binding.root
    }

    private fun alert() = AlertDialog.Builder(context)
    .setTitle("Registrar foto")
    .setMessage("Deseja fotografar o ponto?")
        .setPositiveButton("sim") { dialog, which ->
            take()
        }
    .setNegativeButton("não") { dialog, which ->
        if (binding.left.isChecked) {
            point.side = 1
        } else {
            point.side = 0
        }
        viewModel.createPoint(point, null)
        findNavController().navigateUp()
    }.create().show()

    private  fun take() = if (allPermissionsGranted()) {
        dispatchTakePictureIntent()
    } else {
        ActivityCompat.requestPermissions(
            requireActivity(), REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS
        )
    }


    val takePicture =
        registerForActivityResult(ActivityResultContracts.TakePicture()) {

            if (it) {
                if (binding.left.isChecked) {
                    point.side = 1
                } else {
                    point.side = 0
                }
                viewModel.createPoint(point, photoFile)
                findNavController().navigateUp()
            } else if(viewModel.project.value?.visibility.equals("1")) {
                Snackbar.make(binding.root, "É obrigatório anexar uma foto", Snackbar.LENGTH_LONG)
                    .show()
            }
        }

    var photoFile: File? = null
    lateinit var photoURI: Uri

    private fun dispatchTakePictureIntent() {
        photoFile = try {
            createImageFile()
        } catch (ex: IOException) {

            null
        }
        photoFile?.also {
            try {
                photoURI = FileProvider.getUriForFile(
                    requireContext(),
                    "br.com.seasolutions.seapoint",
                    it
                )
                takePicture.launch(photoURI)
            } catch (err: Exception) {
                Log.e("storageErr", err.toString())
            }

        }
        //viewModel.createPoint(point, photoFile)
    }

    lateinit var currentPhotoPath: String

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val name: String = point.location?.get(0).toString() + "_" +
                point.location?.get(1).toString() + "_" +
                point.location?.get(2).toString() + "_"
        val storageDir: File? = getOutputDirectory()
        return File.createTempFile(
            name, /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

    private val REQUIRED_PERMISSIONS =
        arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE)
    private val REQUEST_CODE_PERMISSIONS = 10
    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            requireContext(), it
        ) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                dispatchTakePictureIntent()
            } else {
                Toast.makeText(
                    requireContext(),
                    "Permissions not granted by the user.",
                    Toast.LENGTH_SHORT
                ).show()

            }
        }
    }

    fun getOutputDirectory(): File {
        val mediaDir = requireActivity().externalMediaDirs.firstOrNull()?.let {
            File(it, "project").apply { mkdirs() }
        }
        return if (mediaDir != null && mediaDir.exists())
            mediaDir else requireActivity().filesDir
    }

}


