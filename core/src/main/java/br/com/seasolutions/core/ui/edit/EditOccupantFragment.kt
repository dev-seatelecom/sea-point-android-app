package br.com.seasolutions.core.ui.edit

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import br.com.seasolutions.core.R
import br.com.seasolutions.core.databinding.ItemSelectList
import br.com.seasolutions.core.databinding.OccupantListUi
import br.com.seasolutions.seapoint.model.Occupant
import br.com.seasolutions.seapoint.ui.create.project.CreateProjectViewModel
import io.realm.RealmList

class EditOccupantFragment : Fragment() {
    private lateinit var searchViewModel: CreateProjectViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bindingUtil = OccupantListUi.inflate(inflater)

        val viewModel = ViewModelProvider(this)[EditOccupantViewModel::class.java]

        var occupants = ArrayList<Occupant>()
        val refresh: SwipeRefreshLayout = bindingUtil.refresh

        refresh.setOnRefreshListener {
            viewModel.getOccupants()
        }


        viewModel.occupants.observe(this.viewLifecycleOwner, androidx.lifecycle.Observer {
            occupants = it
            val occAdapter = Adapter(it)

            bindingUtil.list.adapter = occAdapter
            bindingUtil.list.layoutManager = LinearLayoutManager(requireContext())

            bindingUtil.save.setOnClickListener {
                val occupantsSelected: RealmList<Occupant> = RealmList()
                for (aux in occAdapter.occupants) {
                    if (aux.isChecked) {
                        occupantsSelected.add(aux)
                    }
                }

                viewModel.updateOccupantsOfProject(occupantsSelected)
                Toast.makeText(
                    context,
                    "Os ocupantes do projeto foram alterados com sucesso!",
                    Toast.LENGTH_SHORT
                ).show()
                findNavController().navigateUp()
            }
            refresh.isRefreshing = false

            val ad = bindingUtil.list.adapter as Adapter
            if(viewModel.project.value!=null){
                ad.getCurrentOccupants(viewModel.project.value!!.occupants)
            }
        })

        viewModel.project.observe(this.viewLifecycleOwner, androidx.lifecycle.Observer {

            val ad = bindingUtil.list.adapter as Adapter

            ad.getCurrentOccupants(it!!.occupants)
        })

        return bindingUtil.root
    }

    class Adapter(val occupants: ArrayList<Occupant>) :
        RecyclerView.Adapter<Adapter.AddViewHolder>() {

        class AddViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val binding: ItemSelectList?
            init {
                binding = ItemSelectList.bind(itemView)
            }
        }

        fun getCurrentOccupants(currentOccupants: RealmList<Occupant>){
            for (current in currentOccupants) {
                for ((i, occupant) in occupants.withIndex()) {
                    if (current.name == occupant.name) {
                        occupant.isChecked = true
                    }
                    notifyItemChanged(i)
                }
            }

        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddViewHolder {
            return AddViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_select_list, parent, false)
            )
        }

        override fun getItemCount(): Int {
            return this.occupants.size
        }

        override fun onBindViewHolder(holder: AddViewHolder, position: Int) {
            val occupant = this.occupants.get(position)
            holder.binding!!.option.text = occupant.name
            holder.binding!!.option.isChecked = occupant.isChecked

            holder.binding!!.option.setOnClickListener {
                occupant.isChecked = !occupant.isChecked
                occupants.set(position, occupant)
                notifyItemChanged(position)
            }
        }
    }


}