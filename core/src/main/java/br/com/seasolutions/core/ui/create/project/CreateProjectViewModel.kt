package br.com.seasolutions.seapoint.ui.create.project

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.seasolutions.seapoint.model.Occupant
import br.com.seasolutions.seapoint.model.Project
import br.com.seasolutions.seapoint.repo.db.project.Repository
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.data.model.Token
import io.realm.RealmList
import br.com.seasolutions.seapoint.repo.db.occupant.Repository as OccupantDB
import br.com.seasolutions.seapoint.repo.api.occupant.Repository as OccupantAPI

class CreateProjectViewModel(application: Application) : AndroidViewModel(application) {

    private val _project = MutableLiveData<Project>().apply {
        value = Project()
    }

    val project: LiveData<Project> = _project

    private val _occupants = MutableLiveData<ArrayList<Occupant>>().apply {
        OccupantDB.all(this)
    }

    val occupants: LiveData<ArrayList<Occupant>> = _occupants


    fun setOccupants(list: ArrayList<Occupant>) {
        _occupants.value = list
    }

    fun getOccupants() {
        updateOccupantsOfAPI()
        _occupants.postValue(OccupantDB.getAll())
    }

    fun updateOccupantsOfAPI(){
        OccupantAPI(
            Token().read(getApplication()),
            LoggedInUser().readUser(getApplication())
        ).getAll()
    }


    fun addOccupant(occupant: Occupant) {
        _project.value?.occupants?.add(occupant)
    }

    fun addOccupants(occupants: RealmList<Occupant>) {
        _project.value?.occupants = occupants
    }

    fun setProject(project: Project) {
        _project.value = project
    }
}