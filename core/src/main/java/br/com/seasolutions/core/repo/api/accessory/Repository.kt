package br.com.seasolutions.seapoint.repo.api.accessory

import android.util.Log
import br.com.seasolutions.seapoint.model.AccessoriesService
import br.com.seasolutions.seapoint.model.Accessory
import br.com.seatelecom.auth.data.IP_SERVER
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.data.model.Token
import com.google.gson.GsonBuilder
import io.realm.Realm
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Repository(private val token: Token, private val user: LoggedInUser) {
    private val retrofit = Retrofit.Builder()
        .baseUrl(IP_SERVER)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .build()
    private val webService: AccessoriesService = retrofit.create(
        AccessoriesService::class.java
    )

    fun getAll() {

        if (token != null && user != null) {

            webService.all(user.companies[user.selectedCompany], token.accessToken).enqueue(
                object : Callback<ArrayList<Accessory>> {
                    override fun onResponse(
                        call: Call<ArrayList<Accessory>>,
                        response: Response<ArrayList<Accessory>>
                    ) {
                        if (response.isSuccessful) {
                            response.body()?.let {
                                val realm = Realm.getDefaultInstance()
                                for (aux in it) {
                                    realm.beginTransaction()
                                    realm.insertOrUpdate(aux)
                                    realm.commitTransaction()

                                }
                                realm.close()
                            }
                        }
                    }

                    override fun onFailure(call: Call<ArrayList<Accessory>>, t: Throwable) {
                        Log.e("", t.message.toString())
                    }

                }
            )
        }

    }
}