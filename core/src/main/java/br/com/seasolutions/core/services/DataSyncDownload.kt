package br.com.seasolutions.seapoint.services

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.data.model.Token
import br.com.seasolutions.seapoint.repo.api.accessory.Repository as AccessoryRepository
import br.com.seasolutions.seapoint.repo.api.occupant.Repository as OccupantRepository

class DataSyncDownload(context: Context, workerParams: WorkerParameters) : Worker(context,
    workerParams
) {

    override fun doWork(): Result {
        val token = Token().read(applicationContext)
        val user = LoggedInUser().readUser(applicationContext)
        OccupantRepository(token, user).getAll()
        AccessoryRepository(token, user).getAll()
//        token?.let {
//            user?.let {
//                OccupantRepository.all(token, user)
//                AccessoryRepository().getAll(token, user)
//                ProjectRepository().getAllAPI()
//
//                var list = Realm.getDefaultInstance().where(Project::class.java).findAll()
//                val points = MutableLiveData<ArrayList<Point>>()
//                for (project in list){
//                    PointRepository.get(project.id.toString(), points)
//                }
//
//            }
//        }
        return Result.success()
    }

}