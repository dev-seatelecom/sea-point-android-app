package br.com.seasolutions.seapoint.ui.create.project

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.seasolutions.core.R
import br.com.seasolutions.core.databinding.CheckCreateProjectUI
import br.com.seasolutions.core.databinding.ItemSimpleUI
import br.com.seasolutions.seapoint.model.Occupant
import br.com.seasolutions.seapoint.ui.current.CurrentProjectViewModel
import br.com.seatelecom.auth.login.LoginViewModel
import kotlin.collections.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [CheckCreateFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CheckCreateFragment : Fragment() {
    // TODO: Rename and change types of parameters

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val bindingUtil = CheckCreateProjectUI.inflate(inflater)

        val loginViewModel = ViewModelProvider(requireActivity())
            .get(LoginViewModel::class.java)

        val viewModel = ViewModelProvider(requireActivity())[CreateProjectViewModel::class.java]
        var viewModelCurrent = ViewModelProvider(requireActivity())[CurrentProjectViewModel::class.java]

        viewModel.project.value?.let {
            bindingUtil.name.text = it.name
            bindingUtil.description.text = it.description
            val visibility = resources.getStringArray(R.array.project_visibility)
            bindingUtil.visibility.text = visibility[it.visibility.toInt()]
            val list = ArrayList<Occupant>()
            list.addAll(it.occupants)
            val adapter = Adapter(list)
            bindingUtil.list.adapter = adapter
            bindingUtil.list.layoutManager = LinearLayoutManager(requireContext())
            bindingUtil.save.setOnClickListener {v ->
                viewModelCurrent.createProject(it)
                val action = CheckCreateFragmentDirections
                    .actionDeleteOccupantFragmentToCurrentProjectFragment()
                findNavController().navigate(action)
            }
        }




        return bindingUtil.root

    }

    class Adapter(val occupants: ArrayList<Occupant>): RecyclerView.Adapter<Adapter.AddViewHolder>(){

        class AddViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val binding: ItemSimpleUI?

            init {
                binding = ItemSimpleUI.bind(itemView)
            }

        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddViewHolder {
            return AddViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_simple, parent, false)
            )
        }

        override fun getItemCount(): Int {
            return this.occupants.size
        }

        override fun onBindViewHolder(holder: AddViewHolder, position: Int) {
            val occupant = this.occupants.get(position)
            holder.binding!!.textViewItemSimple.text = occupant.name
        }
    }
}