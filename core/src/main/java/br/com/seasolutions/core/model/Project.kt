package br.com.seasolutions.seapoint.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import org.bson.types.ObjectId
import java.util.*

open class Project(): RealmObject() {
    @PrimaryKey @SerializedName("_id") var id: String = ObjectId().toString()
    var name: String = ""
    var description: String = ""
    var companyId: String = ""
    var operatorId: String = ""
    var visibility: String = ""
    var occupants: RealmList<Occupant> = RealmList<Occupant>()
    var createdAt: Long = Calendar.getInstance().time.time
    var status: String = "toCreate"
    fun createdAtDate(): Calendar {
        val createdAtDate = Calendar.getInstance()
        createdAtDate.time.time = createdAt
        return createdAtDate
    }
}