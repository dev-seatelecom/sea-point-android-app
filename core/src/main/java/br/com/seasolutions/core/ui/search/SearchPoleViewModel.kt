package br.com.seasolutions.seapoint.ui.search

import android.app.Application
import androidx.lifecycle.*
import br.com.seasolutions.seapoint.model.Project
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.data.model.Token
import br.com.seasolutions.seapoint.repo.db.project.Repository as DBProjectRepository
import kotlin.collections.ArrayList
import br.com.seasolutions.seapoint.repo.api.project.Repository as ApiProjectRepository

class SearchPoleViewModel(application: Application) : AndroidViewModel(application) {


    private fun repositoryDB(): DBProjectRepository =
        DBProjectRepository(LoggedInUser().readUser(getApplication()))

    private fun repositoryAPI(): ApiProjectRepository =
        ApiProjectRepository(token = Token().read(getApplication()), user = LoggedInUser().readUser(getApplication()))
    
    private val _update = MutableLiveData<Boolean>().apply { value = true }

    private val _projects =
        MutableLiveData<ArrayList<Project>>().apply {
            repositoryDB().getAll(this)

        }
    val projects: LiveData<ArrayList<Project>> = _projects

    fun update() {
        repositoryAPI().getAllAPI()

    }

    fun updateDB() {
        repositoryDB().getAll(_projects)
    }



}