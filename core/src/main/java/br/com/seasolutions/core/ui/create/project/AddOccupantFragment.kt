package br.com.seasolutions.seapoint.ui.create.project

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import br.com.seasolutions.core.R
import br.com.seasolutions.core.databinding.ItemSelectList
import br.com.seasolutions.core.databinding.ListFragmentUI
import br.com.seasolutions.core.databinding.OccupantListUi
import br.com.seasolutions.seapoint.model.Occupant
import br.com.seatelecom.auth.login.LoginViewModel
import io.realm.RealmList
import kotlin.collections.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [AddOccupantFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AddOccupantFragment : Fragment() {
    // TODO: Rename and change types of parameters

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val bindingUtil = OccupantListUi.inflate(inflater)
        val loginViewModel = ViewModelProvider(requireActivity())
            .get(LoginViewModel::class.java)

        val viewModel = ViewModelProvider(requireActivity())[CreateProjectViewModel::class.java]

        val refresh: SwipeRefreshLayout = bindingUtil.refresh


        refresh.setOnRefreshListener {
            viewModel.getOccupants()
        }

        viewModel.occupants.observe(this.viewLifecycleOwner, androidx.lifecycle.Observer {

            val adapter = Adapter(it)

            bindingUtil.list.adapter = adapter
            bindingUtil.list.layoutManager = LinearLayoutManager(requireContext())
            bindingUtil.save.setOnClickListener {
                viewModel.setOccupants(adapter.occupants)
                val list = RealmList<Occupant>()

                for (aux in adapter.occupants){
                    if (aux.isChecked){
                        //viewModel.addOccupant(aux)
                        list.add(aux)
                    }
                }
                viewModel.addOccupants(list)

                val action =
                    AddOccupantFragmentDirections
                        .actionAddOccupantFragmentToDeleteOccupantFragment()
                findNavController().navigate(action)
            }

            refresh.isRefreshing = false

        })


        return bindingUtil.root
    }


    class Adapter(val occupants: ArrayList<Occupant>): RecyclerView.Adapter<Adapter.AddViewHolder>(){

        class AddViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val binding: ItemSelectList?

            init {
                binding = ItemSelectList.bind(itemView)
            }

        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddViewHolder {
            return AddViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_select_list, parent, false)
            )
        }

        override fun getItemCount(): Int {
            return this.occupants.size
        }

        override fun onBindViewHolder(holder: AddViewHolder, position: Int) {
            val occupant = this.occupants.get(position)
            holder.binding!!.option.text = occupant.name

            holder.binding!!.option.isChecked = occupant.isChecked
            holder.binding!!.option.setOnClickListener {
                occupant.isChecked = !occupant.isChecked
                occupants.set(position, occupant)
                notifyItemChanged(position)
            }
        }
    }
}