package br.com.seasolutions.seapoint.services

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import br.com.seasolutions.core.services.UploadService
import br.com.seasolutions.seapoint.model.Occupant
import br.com.seasolutions.seapoint.model.Point
import br.com.seasolutions.seapoint.model.Project
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.data.model.Token
import br.com.seasolutions.seapoint.repo.api.point.Repository as PointRepository
import br.com.seasolutions.seapoint.repo.api.project.Repository as ProjectRepository
import br.com.seasolutions.seapoint.repo.api.occupant.Repository as OccupantRepository
import io.realm.Realm
import io.realm.kotlin.where
import java.io.File


class DataSyncUpload(context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    override fun doWork(): Result {

        val serviceIntent = Intent(applicationContext, UploadService::class.java)
        applicationContext.startService(serviceIntent)

        return Result.success()
    }
}