package br.com.seasolutions.core.ui.edit

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.seasolutions.seapoint.model.Occupant
import br.com.seasolutions.seapoint.model.Point
import br.com.seasolutions.seapoint.model.Project
import br.com.seasolutions.seapoint.repo.db.occupant.Repository.Companion.getAll
import br.com.seasolutions.seapoint.repo.db.point.Repository
import br.com.seasolutions.seapoint.repo.db.project.Repository as ProjectRepository
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.data.model.Token
import io.realm.RealmList
import br.com.seasolutions.seapoint.repo.api.occupant.Repository as OccupantAPI
import br.com.seasolutions.seapoint.repo.db.occupant.Repository as OccupantRepository
class EditOccupantViewModel(application: Application): AndroidViewModel(application) {

    private val user = LoggedInUser().apply { readUser(getApplication()) }
    private val token = Token().read(getApplication())

    private val repository = ProjectRepository(user)

    private val _project = repository.getCurrent()
    val project:LiveData<Project?> = _project

    val projectEdited = _project.value

    private val _occupants = MutableLiveData<ArrayList<Occupant>>().apply {
        OccupantRepository.all(this)
    }
    val occupants: LiveData<ArrayList<Occupant>> = _occupants

    fun updateOccupantsOfProject(selectedOccupants: RealmList<Occupant>) {
        repository.updateOccupant(_project, selectedOccupants)
        _occupants.value = ArrayList(selectedOccupants)
    }

    fun getOccupants() {
        updateOccupantsOfAPI()
        _occupants.postValue(OccupantRepository.getAll())
    }

    fun updateOccupantsOfAPI(){
        OccupantAPI(
            Token().read(getApplication()),
            LoggedInUser().readUser(getApplication())
        ).getAll()
    }



}