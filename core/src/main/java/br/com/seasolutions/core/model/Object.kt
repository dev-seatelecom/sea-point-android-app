package br.com.seasolutions.core.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import org.bson.types.ObjectId

open class Current(
    var projectId: String = ObjectId().toString(),
    @PrimaryKey var name: String = "current"
): RealmObject()