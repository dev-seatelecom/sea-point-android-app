package br.com.seasolutions.seapoint.ui.current

import android.app.Application
import android.util.Log
import androidx.collection.LruCache
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.seasolutions.seapoint.model.Accessory
import br.com.seasolutions.seapoint.model.Occupant
import br.com.seasolutions.seapoint.model.Project
import br.com.seasolutions.seapoint.model.Point
import br.com.seasolutions.seapoint.repo.db.project.Repository
import br.com.seatelecom.auth.data.LoginDataSource
import br.com.seatelecom.auth.data.LoginRepository
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.data.model.Token
import br.com.seasolutions.seapoint.repo.db.accessory.Repository as AccessoryDB
import br.com.seasolutions.seapoint.repo.db.point.Repository as PointDB
import br.com.seasolutions.seapoint.repo.api.point.Repository as PointAPI
import io.realm.RealmList
import java.io.File

class CurrentProjectViewModel(application: Application) : AndroidViewModel(application) {


    private val user = LoggedInUser().apply { readUser(getApplication()) }
    private val token = Token().read(getApplication())
    private val repository = Repository(user)

    private val _project = repository.getCurrent()
    private val _points = MutableLiveData<ArrayList<Point>>().apply { value = ArrayList() }
    private val _point = MutableLiveData<Point>().apply {
        value = Point()
    }

    private val _occupants = MutableLiveData<ArrayList<Occupant>>().apply {
        reciveOccupants()
    }

    fun updateOccupantsofProject(){
        val list = ArrayList<Occupant>()
        for(aux in _project.value!!.occupants){
            val occupant = Occupant()
            occupant.name = aux.name
            occupant.isChecked = aux.isChecked
            occupant._id = aux._id
            occupant.asn = aux.asn
            occupant.scm = aux.scm
            occupant.status = aux.status
            list.add(occupant)
        }
        _occupants.postValue(list)
    }

    private val _accessories =
        MutableLiveData<ArrayList<Accessory>>().apply {
            AccessoryDB.get(this)
        }


    val points: LiveData<ArrayList<Point>> = _points
    val point: LiveData<Point> = _point
    val occupants: LiveData<ArrayList<Occupant>> = _occupants
    val accessory: LiveData<ArrayList<Accessory>> = _accessories
    val project: LiveData<Project?> = _project

    fun createProject(project: Project){
        repository.create(project)
        setCurrent(project)
        this.onCleared()
    }

    fun refreshProjectInfo(){
        setCurrent(repository.getCurrent().value!!)
    }

    fun reciveOccupants(): ArrayList<Occupant>{
        val list = ArrayList<Occupant>()

        _project.value?.let{

            Log.e("reciveOccupants", "-------------------------")
            for (aux in it.occupants) {
                val occupant = Occupant()
                occupant.name = aux.name
                occupant.isChecked = aux.isChecked
                occupant._id = aux._id
                occupant.asn = aux.asn
                occupant.scm = aux.scm
                occupant.status = aux.status
                list.add(occupant)
                Log.e("", occupant.name)
                Log.e("", occupant.isChecked.toString())
            }
            Log.e("reciveOccupants", "-------------------------")
        }
        return list
    }

    fun syncLocalOccupants(){
       val occupants = _occupants.value
        _occupants.value = reciveOccupants()
        for((index, occ) in occupants!!.withIndex()){
            _occupants.value?.get(index)?.isChecked = occ.isChecked
        }
    }



    fun setCurrent(project: Project){
        _point.value = Point()
        PointDB.get(project.id, _points)
        repository.setCurrent(project)
        _project.value = project
        val list = ArrayList<Occupant>()
        list.addAll(project.occupants)
        _occupants.value = list
    }

    fun refreshPoints(){

        PointAPI.get(_project.value?.id.toString(), _points, token, user)
    }

//    fun getCurrent(){
//        val idProject = _project.value?.id
//        idProject?.let{
//            _project.postValue(repository.get(it))
//        }
//    }

    fun setDefaultPoint(point: Point){
        occupants.value?.let {
            val list = RealmList<Occupant>()
            for(aux in it){
                if(aux.isChecked){
                    list.add(aux)
                }
            }
            point.occupants = list
        }

        accessory.value?.let {
            val list = RealmList<Accessory>()
            for(aux in it){
                if(aux.isChecked){
                    list.add(aux)
                }
            }
            point.accessories = list
        }
        point.projectId = _project.value!!.id
        point.operatorId = user.userId
        _point.value = point
    }

    fun update(project: Project){
      _project.value = repository.update(_project.value!!, project)//corrigir.
    }

    fun delete(project: Project){

        repository.delete(project)
    }

    fun setOccupant(position: ArrayList<Int>){

        val list = _occupants.value!!

        for(aux in position){
            val occupant = list[aux]
            occupant.isChecked = !occupant.isChecked
            list[aux] = occupant
        }

        _occupants.postValue(list)
    }

    fun selectAllOccupants() {
        val list = _occupants.value
        val newList = ArrayList<Occupant>()
        for(aux in list!!){
            aux.isChecked = true
            newList.add(aux)
        }
        //_occupants.value = list
    }

    fun setAccessory(position: ArrayList<Int>){
        val list = _accessories.value
        for(aux in position){
            val occupant = list!!.get(aux)
            occupant.isChecked = !occupant.isChecked
            list.set(aux, occupant)

//            Log.e("ACC", list[aux].toString())
        }
//        Log.e("ACCMAIN", _accessories.value.toString())

    }

    fun selectAllAccessories() {
        val list = _accessories.value
        val newList = ArrayList<Accessory>()
        for(aux in list!!){
            aux.isChecked = true
            newList.add(aux)
        }
        //_accessories.value = list
    }

    fun createPoint(point: Point, file: File?){
        PointDB.create(point, file)
        PointDB.get(point.projectId, _points)
    }

}