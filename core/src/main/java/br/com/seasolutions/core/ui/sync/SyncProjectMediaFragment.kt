package br.com.seasolutions.seapoint.ui.project.sync

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import br.com.seasolutions.core.databinding.SyncProjectMediaUI

class SyncProjectMediaFragment : DialogFragment() {

    companion object {
        fun newInstance() = SyncProjectMediaFragment()
    }

    private lateinit var bind: SyncProjectMediaUI
    private lateinit var viewModel: SyncProjectMediaViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bind = SyncProjectMediaUI.inflate(inflater)
        return bind.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SyncProjectMediaViewModel::class.java)
        bind.progress.progress
    }

}