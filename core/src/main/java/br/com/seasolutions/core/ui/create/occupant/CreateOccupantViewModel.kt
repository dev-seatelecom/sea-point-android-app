package br.com.seasolutions.core.ui.create.occupant

import androidx.lifecycle.ViewModel
import br.com.seasolutions.seapoint.model.Occupant
import br.com.seasolutions.seapoint.repo.db.occupant.Repository as OccupantDB

class CreateOccupantViewModel(): ViewModel() {

    fun createOccupant(occupant: Occupant){
        OccupantDB.create(occupant)
    }
}