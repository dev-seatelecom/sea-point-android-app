package br.com.seasolutions.seapoint.ui.current

import android.Manifest
import android.app.AlertDialog
import br.com.seasolutions.seapoint.ui.current.CurrentProjectFragmentDirections
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import br.com.seasolutions.core.R
import br.com.seasolutions.core.databinding.MapUI
import br.com.seasolutions.seapoint.model.Project
import com.google.android.material.snackbar.Snackbar
import org.osmdroid.api.IMapController
import org.osmdroid.config.Configuration
import org.osmdroid.util.BoundingBox
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.Marker
import java.lang.Exception

class CurrentProjectFragment : Fragment() {

    private lateinit var currentProjectViewModel: CurrentProjectViewModel
    private lateinit var bind: MapUI
    private var location: Location? = null
    private lateinit var locationManager: LocationManager
    private val myLocation: MyLocation = MyLocation()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        currentProjectViewModel =
            ViewModelProvider(requireActivity()).get(CurrentProjectViewModel::class.java)

        val ctx: Context = requireActivity().getApplicationContext()
        Configuration.getInstance().load(ctx, requireActivity().getPreferences(0))
        bind = MapUI.inflate(inflater)
//            DataBindingUtil.inflate(inflater, R.layout.fragment_current_project, container, false)


//        binding.map.setBuiltInZoomControls(false)
        bind.map.zoomController.onDetach()
        bind.map.setMultiTouchControls(true)


        val mapController: IMapController = bind.map.getController()
        mapController.zoomTo(16.0)

        val activity = (requireActivity() as AppCompatActivity)
        var actionBar = activity.supportActionBar

        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 0
        )

        currentProjectViewModel.project.observe(viewLifecycleOwner, Observer {
            actionBar?.title = it!!.name
            setHasOptionsMenu(true)

        })

        currentProjectViewModel.points.observe(this.viewLifecycleOwner, Observer {

            bind.refreshPoints.visibility = View.GONE

            if (bind.map.overlays.size != 0) {
                val pointLocation = bind.map.overlays.get(0)
                bind.map.overlays.clear()
                bind.map.overlays.add(0, pointLocation)
            } else {
                val pointFirst = Marker(bind.map)
                pointFirst.icon = ContextCompat.getDrawable(requireActivity(), R.drawable.ic_null)
                bind.map.overlays.add(0, pointFirst)

            }
            val geoPointList = arrayListOf<GeoPoint>()
            for (pt in it) {
                val point = Marker(bind.map)
                val geoPoint =
                    GeoPoint(pt.location?.get(0)!!, pt.location?.get(1)!!, pt.location?.get(2)!!)
                geoPointList.add(geoPoint)
                point.position = geoPoint
                point.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
                point.icon = ContextCompat.getDrawable(requireActivity(), R.drawable.ic_street)
//                point.setOnMarkerClickListener { marker, mapView ->
//                    return@setOnMarkerClickListener true
//                }
                bind.map.overlays.add(point)

//                mapController.animateTo(geoPoint)
                val area = BoundingBox.fromGeoPointsSafe(geoPointList)
                bind.map.zoomToBoundingBox(area, false, 0, 16.0, 0)
            }

        })

        bind.add.setOnClickListener {

            location?.let {

                bind.map.getController().animateTo(GeoPoint(it.latitude, it.longitude))


                if (currentProjectViewModel.point.value?.type != "") {
                    findNavController().navigate(
                        CurrentProjectFragmentDirections
                            .actionCurrentProjectFragmentToCreatePointFragment(it)
                    )
                } else {
                    Snackbar.make(
                        bind.config,
                        "É preciso configurar o ponto antes de começar a criar",
                        Snackbar.LENGTH_LONG
                    ).show()
                }
            } ?: run {
                Snackbar.make(
                    bind.config, "Espere carregar a localização para criar um novo poste",
                    Snackbar.LENGTH_LONG
                ).show()
            }
        }

        bind.config.setOnClickListener {
            val action = CurrentProjectFragmentDirections
                .actionCurrentProjectFragmentToConfigPointFragmen()
            findNavController().navigate(action)
        }


//        requireActivity().menuInflater.inflate(R.menu.current_project,bind.actionMenuView.menu)
//        bind.toolbar.setOnMenuItemClickListener {
//
//            return@setOnMenuItemClickListener true
//        }
        return bind.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.current_project, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.refresh_points -> {
                bind.refreshPoints.visibility = View.VISIBLE
                currentProjectViewModel.refreshPoints()
                true
            }
            R.id.edit_project -> {
                val action =
                    CurrentProjectFragmentDirections.actionCurrentProjectFragmentToEditProjectFragment()
                findNavController().navigate(action)
                true
            }
            R.id.edit_occupants -> {
                val action =
                    CurrentProjectFragmentDirections.actionCurrentProjectFragmentToEditOccupantFragment()
                findNavController().navigate(action)
                true
            }
            R.id.delete_project -> {
                alertToDelete()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        bind.map.onResume()
        getLocation(bind.root)
    }

    override fun onPause() {
        super.onPause()
        bind.map.onPause()
        locationManager.removeUpdates(myLocation)
    }

    private fun alertToDelete() = AlertDialog.Builder(context)
        .setTitle("Excluir projeto")
        .setMessage("Deseja excluir este projeto?")
        .setCancelable(false)
        .setNegativeButton("não") { dialog, which ->
        }
        .setPositiveButton("sim") { dialog, which ->
            deleteCurrentProject()
            findNavController().navigateUp()
        }.create().show()

    private fun deleteCurrentProject() {
        currentProjectViewModel.project.value?.apply {
            currentProjectViewModel.delete(this)

            Toast.makeText(
                context,
                "O projeto foi deletado com suceso!",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    fun getLocation(view: View) {
        try { //Checa as permissões e inicia o escutador da localização.
            location(view)
        } catch (ex: SecurityException) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 0
            )  //pede a permissão ao usuário caso esteja negado
        } finally {
            location(view)
        }
    }


    fun location(view: View) {
        locationManager =
            requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        locationManager.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            50,
            1f,
            myLocation
        ) //inicia o escutador do gps
//        } else {
//            Snackbar.make(
//                view,
//                "O GPS deve estar ligado para coletar a localização da falha",
//                Snackbar.LENGTH_LONG
//            ).show()
//        }
    }


    inner class MyLocation : LocationListener {
        override fun onLocationChanged(it: Location) {
            val startPoint = GeoPoint(it!!.latitude, it.longitude)

            location = it // Variável que será inicializada e bloqueia a criação de pontos se o GPS


            // Trecho responsável por criar no mapa o ícone com o ponto que o GPS localizou.
            val point = Marker(bind.map)
            point.position = startPoint
            point.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
            point.icon = ContextCompat.getDrawable(requireActivity(), R.drawable.ic_location)

            if (bind.map.overlays.size == 0) {
                bind.map.overlays.add(point)
            } else {
                bind.map.overlays.removeAt(0)
                bind.map.overlays.add(0, point)
            }

            if (bind.currentLocation.isChecked) {
                bind.map.getController().animateTo(startPoint)
            }
        }

        override fun onProviderDisabled(provider: String) {

        }

        override fun onProviderEnabled(provider: String) {

        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
            //Toast.makeText(context, "testando", Toast.LENGTH_SHORT).show()
            var s = ""
        }

    }
}
