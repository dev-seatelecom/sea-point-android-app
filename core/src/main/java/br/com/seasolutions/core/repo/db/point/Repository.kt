package br.com.seasolutions.seapoint.repo.db.point

import android.util.Log
import androidx.lifecycle.MutableLiveData
import br.com.seasolutions.seapoint.model.Point
import io.realm.Realm
import java.io.File

class Repository(){

    companion object{
        fun create(point: Point, file: File?){
            val realm = Realm.getDefaultInstance()
            realm.executeTransaction {

                point.file = file?.name
                it.insertOrUpdate(point)
            }
            realm.close()
        }

        fun update(point: Point){
            val realm = Realm.getDefaultInstance()
            point.status = "toUpdate"
            realm.executeTransaction {
                it.insertOrUpdate(point)
            }
            realm.close()
        }

        fun delete(point: Point){
            val realm = Realm.getDefaultInstance()
            point.status = "toDelete"
            realm.executeTransaction {
                it.insertOrUpdate(point)
            }
        }

        fun get(projectId: String, points: MutableLiveData<ArrayList<Point>>){
            val realm = Realm.getDefaultInstance()
            val equalTo =
                realm.where(Point::class.java).equalTo("projectId", projectId).findAll()
            val list = ArrayList<Point>()
            list.addAll(equalTo)
            points.value = list

            equalTo.addChangeListener { equalTo, t->
                list.addAll(equalTo)
                points.value = list
            }

        }
    }

}