package br.com.seasolutions.seapoint.repo.db.occupant

import android.util.Log
import androidx.lifecycle.MutableLiveData
import br.com.seasolutions.seapoint.model.Occupant
import io.realm.Realm
import org.bson.types.ObjectId
import java.lang.Error
import java.lang.Exception

class Repository {
    companion object {

        fun get(id: ObjectId): Occupant = Realm.getDefaultInstance()
            .where(Occupant::class.java).equalTo("_id", id)
            .findFirstAsync()

        fun all(occupants: MutableLiveData<ArrayList<Occupant>>) {
            val realm = Realm.getDefaultInstance()
            val result = realm.where(Occupant::class.java).findAll()
            occupants.value = ArrayList(result)
            realm.close()
        }

        fun getAll(): ArrayList<Occupant>{
            val realm = Realm.getDefaultInstance()
            val result = realm.where(Occupant::class.java).findAll()
            val aux = ArrayList<Occupant>()
            aux.addAll(result)
            return aux
        }

        fun create(occupant: Occupant) {
            val realm = Realm.getDefaultInstance()
            realm.executeTransaction {
                it.insertOrUpdate(occupant)

            }
        }
    }
}