package br.com.seasolutions.seapoint.model

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import org.bson.types.ObjectId
import java.util.*

open class Point(): RealmObject() {

    var location: RealmList<Double> = RealmList()
    var accuracy: Float = 0f
    var createdAt: Long = Calendar.getInstance().time.time
    var type: String = ""
    var projectId: String = ""
    var operatorId: String = ""
    var capacity: Int = 150
    var distance: Int = 5
    var height: Int = 7
    var side: Int = 0
    var accessories: RealmList<Accessory> = RealmList()
    var occupants: RealmList<Occupant> = RealmList()
    var file: String? = ""
    var status: String = "toCreate"
    @PrimaryKey var _id : String = ObjectId().toString()

    fun getJson():JsonObject{

        val json = JsonObject()

        val arrayLocation = JsonArray()
        for (aux in location){
            arrayLocation.add(aux)
        }

        val arrayAccessory = JsonArray()
        for (aux in accessories){
            arrayAccessory.add(aux.getJson())
        }

        val arrayOccupant = JsonArray()
        for (aux in occupants){
            arrayOccupant.add(aux.getJson())
        }

        json.add("location", arrayLocation)
        json.addProperty("accuracy", accuracy)
        json.addProperty("createdAt", createdAt)
        json.addProperty("type", type)
        json.addProperty("operatorId", operatorId)
        json.addProperty("capacity", capacity)
        json.addProperty("distance", distance)
        json.addProperty("height", height)
        json.addProperty("side", side)
        json.add("accessories", arrayAccessory)
        json.add("occupants", arrayOccupant)
        json.addProperty("_id", _id.toString())
        json.addProperty("file", file)

        return json
    }

}