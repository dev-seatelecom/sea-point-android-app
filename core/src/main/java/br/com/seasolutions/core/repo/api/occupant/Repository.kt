package br.com.seasolutions.seapoint.repo.api.occupant

import br.com.seasolutions.seapoint.model.OccupantsService
import br.com.seasolutions.seapoint.model.Occupant
import br.com.seasolutions.seapoint.model.Project
import br.com.seatelecom.auth.data.IP_SERVER
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.data.model.Token
import com.google.gson.GsonBuilder
import io.realm.Realm
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Repository(private val token: Token, private val user: LoggedInUser) {

    private val retrofit = Retrofit.Builder()
        .baseUrl(IP_SERVER)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .build()
    private val webService: OccupantsService = retrofit.create(
        OccupantsService::class.java
    )

    fun getAll() {

        webService.all(user.companies[user.selectedCompany], token.accessToken)
            .enqueue(object : Callback<ArrayList<Occupant>> {
                override fun onFailure(call: Call<ArrayList<Occupant>>, t: Throwable) {
                    print(t)
                }

                override fun onResponse(
                    call: Call<ArrayList<Occupant>>,
                    response: Response<ArrayList<Occupant>>
                ) {
                    if (response.isSuccessful) {
                        response.body()?.let {
                            val realm = Realm.getDefaultInstance()
                            for (aux in it) {
                                realm.beginTransaction()
                                realm.insertOrUpdate(aux)
                                realm.commitTransaction()

                            }
                            realm.close()
                            print("")
                        }
                    }
                }
            })
    }

    fun create(occupant: Occupant): Boolean {
        val json = occupant.getJson()
        val response = webService.create(user.companies[user.selectedCompany], token.accessToken, json)
            .execute()

        if (response.isSuccessful.or(response.code()==600)) {
            try {
                val realm = Realm.getDefaultInstance()
                response.body()?.let { occ ->
                    realm.executeTransaction {
                        occ.status = "created"
                    }
                }
                realm.close()
                return true
            } catch (e: IllegalStateException){
                return false
            }
        } else return false

    }

    fun unpackege(aux: Occupant): Occupant {
        val occupant = Occupant()

        occupant.name = aux.name
        occupant.asn = aux.asn
        occupant.scm = aux.scm
        occupant._id = aux._id
        occupant.status = aux.status
        occupant.isChecked = aux.isChecked

        return occupant
    }

//        fun get(token: Token?, user: LoggedInUser? = null){
//            token?.let{
//                user?.let{
//                    webService.create().enqueue(object : Callback<Occupant>{
//                        override fun onResponse(
//                            call: Call<Occupant>,
//                            response: Response<Occupant>
//                        ) {
//                            if(response.isSuccessful){
//                                response.body()?.let{
//
//                                }
//                            }
//                        }
//
//                        override fun onFailure(call: Call<Occupant>, t: Throwable) {
//                            print(t)
//                        }
//
//                    })
//                }
//            }
//        }


}