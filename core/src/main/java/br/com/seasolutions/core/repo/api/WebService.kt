package br.com.seasolutions.seapoint.model

import com.google.gson.JsonObject
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*
import kotlin.collections.ArrayList


interface ProjectService{
    @POST("projects")
    fun create(
        @Query("access_token") token: String?,
        @Query("company") company: String?,
        @Body project: Project
    ): Call<Project>

    @PUT("project/{projectId}")
    fun update(
        @Path("projectId") projectId: String,
        @Query("access_token") token: String?,
        @Query("companyId") company: String?,
        @Body project: Project
    ): Call<Project>

    @DELETE("project/{projectId}")
    fun delete(
        @Path("projectId") projectId: String,
        @Query("access_token") token: String?,
        @Query("company") company: String?
    ): Call<Project>

    @GET("projects")
    fun getAllProjects(
        @Query("access_token") token: String?,
        @Query("company") company: String?
    ): Call<ArrayList<Project>>
}

interface PointService{
    @POST("project/{projectId}/points")
    fun create(
        @Path("projectId") projectId: String,
        @Query("access_token")         token: String?,
        @Query("company") company: String?,
        @Body point: JsonObject
    ): Call<Point>

    @Multipart
    @POST("project/{projectId}/points/{pointId}/file")
    fun create(
        @Path("projectId") projectId: String,
        @Path("pointId") pointId: String,
        @Query("access_token")         token: String?,
        @Query("company") company: String?,
        @Part imageFile: MultipartBody.Part
    ): Call<Point>

    @GET("project/{projectId}/points")
    fun get(
        @Path("projectId") projectId: String,
        @Query("company") company: String?,
        @Query("access_token") token: String?
    ): Call<ArrayList<Point>>

    @DELETE("project/{projectId}/points/{point}")
    fun delete(
        @Path("projectId") projectId: String,
        @Query("access_token") token: String?,
        @Query("company") company: String?,
        @Path("point") pointId: String
    ): Call<Point>

    @PUT("project/{projectId}/points/{point}")
    fun update(
        @Path("projectId") projectId: String,
        @Query("access_token")         token: String?,
        @Query("company") company: String?,
        @Path("point") pointId: String,
        @Body data: Point
    ): Call<Point>
}

interface OccupantsService{

    @GET("occupant")
    fun all(
        @Query("company") company: String?,
        @Query("access_token") token: String?
    ): Call<ArrayList<Occupant>>

    @POST("occupant")
    fun create(
        @Query("company") company: String?,
        @Query("access_token") token: String?,
        @Body occupant: JsonObject

    ): Call<Occupant>
}

interface AccessoriesService{
    @GET("accessories")
    fun all(
        @Query("company") company: String?,
        @Query("access_token") token: String?
    ): Call<ArrayList<Accessory>>
}

interface UserService {

    @GET("user/info")
    fun infoUser(
        @Query("access_token") token: String?
    ): Call<JsonObject>
}
