package br.com.seasolutions.seapoint.repo.api.project

import android.util.Log
import br.com.seasolutions.seapoint.model.ProjectService
import br.com.seasolutions.seapoint.model.Occupant
import br.com.seasolutions.seapoint.model.Project
import br.com.seatelecom.auth.data.IP_SERVER
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.data.model.Token
import io.realm.Realm
import io.realm.kotlin.delete
import io.realm.kotlin.where
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Repository(private val token: Token, private val user: LoggedInUser) {

    private val retrofit = Retrofit.Builder()
        .baseUrl(IP_SERVER)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    private val webService: ProjectService = retrofit.create(
        ProjectService::class.java
    )

    fun create(project: Project): Boolean {

        val response = webService.create(
            token.accessToken,
            user.companies[user.selectedCompany],
            unpackege(project)
        ).execute()

        if (response.isSuccessful.or(response.code() == 600)) {
            try {
                val realm = Realm.getDefaultInstance()
                realm.executeTransaction {
                    val proj = realm.where(Project::class.java)
                        .equalTo("id", project.id).findFirst()

                    proj?.status = "created"
                    Log.e("Proj - api 46", proj!!.status)
                }

                realm.close()
                return true
            } catch (e: IllegalStateException) {
                return false
            }
        } else return false
    }

    fun update(project: Project): Boolean {

        val response = webService.update(
            project.id,
            token.accessToken,
            user.companies[user.selectedCompany],
            unpackege(project)
        ).execute()
        val realm = Realm.getDefaultInstance()
        if (response.isSuccessful) {
            response.body()?.let {
                it.status = "created"
                realm.executeTransaction { it2 ->
                    it2.insertOrUpdate(it)
                }
                Log.e("Proj - api 70", it.status)
                return true
            }
            return false
        } else {

            realm.executeTransaction {
                it.insertOrUpdate(project)
            }
            return false
        }
    }

    fun delete(project: Project): Boolean {
        val response = webService.delete(
            project.id,
            token.accessToken,
            user.companies[user.selectedCompany],
        ).execute()

        val realm = Realm.getDefaultInstance()
        if (response.isSuccessful) {
            response.body()?.let {
                realm.executeTransaction { it2 ->
                    project.status = "deleted"
                }
              //  Log.e("Proj Delete - api 97", project.status)
                return true
            }
            return false
        } else {
            return false
        }

    }


    fun unpackege(aux: Project): Project {
        val project = Project()
        project.visibility = aux.visibility
        project.companyId = aux.companyId
        project.createdAt = aux.createdAt
        project.description = aux.description
        project.operatorId = aux.operatorId

        for (auxProject in aux.occupants) {
            val occupant = Occupant()
            occupant.name = auxProject.name
            occupant.isChecked = auxProject.isChecked
            occupant._id = auxProject._id
            occupant.asn = auxProject.asn
            occupant.scm = auxProject.scm
            occupant.status = auxProject.status
            project.occupants.add(occupant)
        }
        project.status = aux.status
        project.name = aux.name
        project.id = aux.id
        return project
    }

    fun getAllAPI() {
        webService.getAllProjects(token.accessToken, user.companies[user.selectedCompany])
            .enqueue(object : Callback<ArrayList<Project>> {
                override fun onFailure(call: Call<ArrayList<Project>>, t: Throwable) {
                }

                override fun onResponse(
                    call: Call<ArrayList<Project>>,
                    response: Response<ArrayList<Project>>
                ) {
                    if (response.isSuccessful) {
                        val realm = Realm.getDefaultInstance()
                        response.body()?.let {
                            for (aux in it) {
                                realm.beginTransaction()
                                realm.insertOrUpdate(aux)
                                realm.commitTransaction()
                            }
                            realm.close()
                        }
                    }
                }
            })
    }
}