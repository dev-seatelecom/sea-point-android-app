package br.com.seasolutions.core.ui.create.occupant

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import br.com.seasolutions.core.R
import br.com.seasolutions.core.databinding.CreateOccupantUI
import br.com.seasolutions.seapoint.model.Occupant

class createOccupantFragment : Fragment() {
    private lateinit var binding: CreateOccupantUI
    private lateinit var createOccupantViewModel: CreateOccupantViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_create_occupant,
            container,
            false
        )

        val activity = (requireActivity() as AppCompatActivity)
        var actionBar = activity.supportActionBar
        actionBar?.title = "Adicionar Ocupante"
        setHasOptionsMenu(true)

        createOccupantViewModel =
            ViewModelProvider(requireActivity())[CreateOccupantViewModel::class.java]

        binding.save.setOnClickListener {


            if (binding.name.text?.length != 0) {
                if (binding.asn.text?.length != 0) {
                    if (binding.scm.text?.length != 0) {
                        val occupant = Occupant()

                        occupant.name = binding.name.text.toString()
                        occupant.asn = binding.asn.text.toString()
                        occupant.scm = binding.scm.text.toString()

                        createOccupantViewModel.createOccupant(occupant)
                        Toast.makeText(
                            context,
                            "Ocupante criado com sucesso!",
                            Toast.LENGTH_SHORT
                        ).show()

                        findNavController().popBackStack()


                    } else {
                        binding.scm.error = "Campo obrigatório"
                        binding.scm.requestFocus()
                    }
                } else {
                    binding.asn.error = "Campo obrigatório"
                    binding.asn.requestFocus()
                }
            } else {
                binding.name.error = "Campo obrigatório"
                binding.name.requestFocus()
            }
        }

        return binding.root
    }

}