package br.com.seasolutions.seapoint.ui.search

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.UserHandle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.ListFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.work.*
import br.com.seasolutions.core.R
import br.com.seasolutions.core.databinding.ItemCallList
import br.com.seasolutions.core.databinding.ListFragmentUI
import br.com.seasolutions.seapoint.model.Project
import br.com.seasolutions.seapoint.services.DataSyncDownload
import br.com.seasolutions.seapoint.services.DataSyncUpload
import br.com.seasolutions.seapoint.ui.current.CurrentProjectViewModel
import br.com.seasolutions.seapoint.ui.search.SearchPoleFragmentDirections
import br.com.seatelecom.auth.login.LoginViewModel
import br.com.seatelecom.auth.login.LoginViewModelFactory
import com.google.android.material.transition.MaterialSharedAxis
import java.time.Duration
import java.util.concurrent.TimeUnit
import android.view.MenuInflater
import androidx.fragment.app.FragmentManager
import androidx.navigation.fragment.NavHostFragment
import br.com.seasolutions.seapoint.ui.create.project.CreateProjectViewModel
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.data.model.Token


class SearchPoleFragment : ListFragment(), AdapterView.OnItemClickListener {

    private lateinit var logginViewModel: LoginViewModel
    private lateinit var viewModel: SearchPoleViewModel
    private lateinit var currentProjectViewModel: CurrentProjectViewModel
    private lateinit var projectViewModel: CreateProjectViewModel
    private lateinit var dataBinder: ListFragmentUI
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val forward = MaterialSharedAxis(MaterialSharedAxis.X, true)
        enterTransition = forward

        val backward = MaterialSharedAxis(MaterialSharedAxis.X, false)
        returnTransition = backward
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinder = ListFragmentUI.inflate(inflater)

        try {
            val token = Token().read(requireContext())
            val user = LoggedInUser().readUser(requireContext())
            if (token.accessToken != null && user.name != ""){
                    (requireActivity() as AppCompatActivity).supportActionBar?.show()
                logginViewModel = ViewModelProvider(
                    requireActivity(),
                    LoginViewModelFactory(requireActivity().application)
                ).get(LoginViewModel::class.java)
                viewModel = ViewModelProvider(this)[SearchPoleViewModel::class.java]
                currentProjectViewModel =
                    ViewModelProvider(requireActivity()).get(CurrentProjectViewModel::class.java)
                projectViewModel =
                    ViewModelProvider(requireActivity())[CreateProjectViewModel::class.java]

                val refresh: SwipeRefreshLayout = dataBinder.refresh
                val act = (requireActivity() as AppCompatActivity).supportActionBar

                act?.title = "Lista de Projetos"
                setHasOptionsMenu(true)

                refresh.setOnRefreshListener {
                    viewModel.update()
                }
                viewModel.projects.observe(this.viewLifecycleOwner, Observer {
                    val adapter = Adapter(requireContext(), it)
                    listAdapter = adapter
                    listView.onItemClickListener = this
                    refresh.isRefreshing = false
                })

                periodicRequests()
            }else{
                logout()
            }
        } catch (e: Exception) {
            logout()
        }



        return dataBinder.root
    }

    fun logout() {
        val uri = Uri.parse("auth://loginFragment")
        NavHostFragment.findNavController(this).navigate(uri)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.start_project, menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.createProjectFragment -> {

                for (occ in projectViewModel.occupants.value!!) {
                    occ.isChecked = false
                }

                val action =
                    SearchPoleFragmentDirections.actionSearchPoleFragmentToCreateProjectFragment()
                findNavController().navigate(action)
                true
            }
//            R.id.createOccupantFragment -> {
//                val action =
//                    SearchPoleFragmentDirections.actionSearchPoleFragmentToCreateOccupantFragment5()
//                findNavController().navigate(action)
//                true
//            }

            R.id.loginFragment -> {
                val user = LoggedInUser()
                user.writeUser(requireContext())
                val token = Token()
                token.write(requireContext())
                //NavHostFragment.findNavController(this).popBackStack()
                //NavHostFragment.findNavController(this).navigateUp()
                logout()

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun periodicRequests() {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val upload = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            PeriodicWorkRequestBuilder<DataSyncUpload>(900000, TimeUnit.MILLISECONDS)
                .setConstraints(constraints)
                .setInitialDelay(Duration.ZERO)

                .addTag("UPLOAD")
                .build()
        } else {
            PeriodicWorkRequestBuilder<DataSyncUpload>(900000, TimeUnit.MILLISECONDS)
                .setConstraints(constraints)
                .addTag("UPLOAD")
                .setInitialDelay(0, TimeUnit.MILLISECONDS)
                .build()
        }

        val download = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            PeriodicWorkRequestBuilder<DataSyncDownload>(900000, TimeUnit.MILLISECONDS)
                .addTag("DOWNLOAD")
                .setInitialDelay(Duration.ZERO)
                .build()
        } else {
            PeriodicWorkRequestBuilder<DataSyncDownload>(900000, TimeUnit.MILLISECONDS)
                .addTag("DOWNLOAD")
                .setInitialDelay(0, TimeUnit.MILLISECONDS)
                .build()
        }

        WorkManager.getInstance(requireContext()).enqueueUniquePeriodicWork(
            "UPLOAD",
            ExistingPeriodicWorkPolicy.KEEP, upload
        )
        val resultDownload = WorkManager.getInstance(requireContext()).enqueueUniquePeriodicWork(
            "DOWNLOAD",
            ExistingPeriodicWorkPolicy.KEEP, download
        )
    }



    class Adapter(context: Context, val projects: ArrayList<Project>) :
        ArrayAdapter<Project>(context, R.layout.item_simple_list, projects) {
        class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val binding: ItemCallList?

            init {
                binding = ItemCallList.bind(itemView)
            }

        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val holder = Holder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_simple_list, parent, false)
            )
            val project = projects.get(position)
            holder.binding!!.protocol.text = project.name
            holder.binding!!.address.text = project.description
            return holder.itemView
        }
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        if (ActivityCompat.checkSelfPermission(//Verifica se tem permissão
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {


            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 0
            )  //pede a permissão ao usuário caso esteja negado


        } else {
            currentProjectViewModel.setCurrent(viewModel.projects.value!!.get(position))
            val action =
                SearchPoleFragmentDirections.actionSearchPoleFragmentToCurrentProjectFragment()
            findNavController().navigate(action)
        }


    }
}