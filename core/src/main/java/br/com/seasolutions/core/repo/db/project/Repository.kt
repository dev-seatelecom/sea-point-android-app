package br.com.seasolutions.seapoint.repo.db.project


import android.util.Log
import androidx.lifecycle.MutableLiveData
import br.com.seasolutions.core.model.Current
import br.com.seasolutions.seapoint.model.Accessory
import br.com.seasolutions.seapoint.model.Occupant
import br.com.seasolutions.seapoint.model.Project
import br.com.seatelecom.auth.data.model.LoggedInUser as User
import io.realm.Realm
import io.realm.RealmList
import io.realm.kotlin.where

class Repository(private val user: User) {

    fun getAccessories(projectId: String): ArrayList<Accessory> {
        val list = ArrayList<Accessory>()
        val realm = Realm.getDefaultInstance()
        val result = realm.where<Accessory>().equalTo("projectId", projectId).findAll()
        list.addAll(result)

        return list
    }


    fun setAccessory(projectId: String, name: String) {
        val realm = Realm.getDefaultInstance()
        val acc = Accessory(projectId, name)
        realm.executeTransaction {
            it.copyToRealmOrUpdate(acc)
        }

    }

    fun setOccupant(projectId: String, name: String) {
        val realm = Realm.getDefaultInstance()
        val acc = Occupant(projectId, name)
        realm.executeTransaction {
            it.copyToRealmOrUpdate(acc)
        }
    }

    fun getOccupants(projectId: String): ArrayList<Occupant> {
        val list = ArrayList<Occupant>()
        val realm = Realm.getDefaultInstance()
        val result = realm.where<Occupant>().equalTo("projectId", projectId).findAll()

        list.addAll(result)

        return list
    }

    fun create(project: Project) {
        val realm = Realm.getDefaultInstance()
        realm.executeTransaction {
            //it.copyToRealm(project)
            it.insertOrUpdate(project)
        }
    }

    fun delete(project: Project) {
        val realm = Realm.getDefaultInstance()

        realm.executeTransaction {
            project.status = "toDelete"
        }
       // Log.e("Proj delete - db 67", project.status)
    }

    fun update(old: Project, new: Project): Project {
        val realm = Realm.getDefaultInstance()

        realm.executeTransaction {
            if (old.isValid) {
                old.status = new.status
                old.visibility = new.visibility
                old.description = new.description
                old.name = new.name

            }

        }
        Log.e("Proj - db 82", old.status)
        return old
    }

    fun get(idProject: String): Project {
        val realm = Realm.getDefaultInstance()
        val project = realm.where<Project>()
            .equalTo("id", idProject).findFirst()
        project?.let { aux ->
            return aux
        } ?: run {
            return Project()
        }
    }


    fun getAll(
        _projects: MutableLiveData<ArrayList<Project>>
    ) {

        val realm = Realm.getDefaultInstance()

        val list = ArrayList<Project>()
        val projects =
            realm.where<Project>().equalTo("companyId", user.companies[user.selectedCompany]!!)
                .findAll()

        for (project in projects) {
            if (project.status != "deleted" && project.status != "toDelete") {
                list.add(project)
            }
        }

        _projects.value = list
        realm.addChangeListener {
            val list = ArrayList<Project>()
            val projects =
                realm.where<Project>().equalTo("companyId", user.companies[user.selectedCompany]!!)
                    .findAll()

            for (project in projects) {
                if (project.status != "deleted" && project.status != "toDelete") {
                    list.add(project)
                }
            }
            _projects.value = list
        }

    }


    fun setCurrent(project: Project) {
        val realm = Realm.getDefaultInstance()
        realm.executeTransaction {
            it.copyToRealmOrUpdate(Current(project.id))
        }
    }

    fun getCurrent(): MutableLiveData<Project?> {
        val realm = Realm.getDefaultInstance()
        val project = MutableLiveData<Project?>()
        val current =
            realm.where<Current>().findFirst()
        if (current != null) {
            val auxProject = get(current.projectId)
            project.postValue(auxProject)
//            auxProject.addChangeListener<Project> { pro ->
//                project.postValue(pro)
//            }
            project.postValue(auxProject)
        } else {
            project.postValue(null)
        }

        return project
    }

    fun updateOccupant(
        _project: MutableLiveData<Project?>,
        selectedOccupants: RealmList<Occupant>
    ) {
        val realm = Realm.getDefaultInstance()
        val current = _project.value!!
        realm.executeTransaction {
            if (current.isValid) {
                _project.value!!.occupants = selectedOccupants
                _project.value!!.status = "toUpdate"
            }
        }
        for(aux in _project.value!!.occupants){
            Log.e("OCCPROJECT", aux.name)
        }

        for(aux in selectedOccupants){
            Log.e("OCCSELECTED", aux.name)
        }
    }

}