package br.com.seasolutions.seapoint.model

import io.realm.RealmObject

open class Photo(
    var filePath: String? = "",
    var name: String? = ""
): RealmObject()