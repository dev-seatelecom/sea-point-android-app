package br.com.seasolutions.seapoint.repo.api.point

import android.util.Log
import androidx.lifecycle.MutableLiveData
import br.com.seatelecom.auth.data.IP_SERVER
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.data.model.Token
import br.com.seasolutions.seapoint.model.Point
import br.com.seasolutions.seapoint.model.PointService
import br.com.seasolutions.seapoint.model.Project
import br.com.seatelecom.auth.data.IP_SERVER
import io.realm.Realm
import io.realm.kotlin.where
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.io.FileNotFoundException

class Repository() {

    companion object {
        private val retrofit = Retrofit.Builder()
            .baseUrl(IP_SERVER)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        private val webService: PointService = retrofit.create(
            PointService::class.java
        )

        fun create(pointId: String, file: File?, token: Token?, user: LoggedInUser): Boolean {
            token?.let {
                val point = Realm.getDefaultInstance().where(Point::class.java)
                    .equalTo("_id", pointId).findFirst()!!
                val json = point.getJson()
                val response = webService.create(
                    point.projectId,
                    token.accessToken,
                    user!!.companies[user!!.selectedCompany],
                    json
                )
                    .execute()
                Log.e("Point", response.code().toString())
                if (response.isSuccessful) {
                    val realm = Realm.getDefaultInstance()

                    val pointAux = realm.where(Point::class.java)
                        .equalTo("_id", response.body()!!._id).findFirst()

                    val project = realm.where(Project::class.java)
                        .equalTo("id", response.body()!!.projectId).findFirst()

                    realm.executeTransaction {
                        if(project!!.visibility == "0" && point.file == null){
                            pointAux!!.status = "created"
                        }
                        if(point.file!=null){
                            pointAux!!.status = "notUploadPhoto"

                        }
                    }
                    Log.e("Point create api 66 - ", pointAux!!.status)
                    return true
                } else return false
            }
            return false
        }


        fun uploadFile(point: Point, file: File?, token: Token?, user: LoggedInUser): Boolean {


            try {
                val body =
                    MultipartBody.Part.createFormData(
                        "file",
                        file!!.name,
                        RequestBody.create(MediaType.parse("image/jpg"), file)
                    )

                token?.let {

                    val response = webService.create(
                        point.projectId,
                        point._id,
                        token.accessToken,
                        user.companies[user.selectedCompany],
                        body
                    )
                        .execute()
                    if (response.isSuccessful) {
                        val realm = Realm.getDefaultInstance()
                        realm.executeTransaction {
                            val pointAux = response.body()
                            pointAux?.status = "created"
                            val s = ""
                        }
                        return true
                    } else return false
                }
            } catch (err: FileNotFoundException) {
                Log.e("err", err.toString())
                return false
            }

            return false
        }


        fun get(
            projectId: String,
            points: MutableLiveData<ArrayList<Point>>,
            token: Token?,
            user: LoggedInUser
        ) {

            token?.let {

                webService.get(
                    projectId,
                    user!!.companies[user!!.selectedCompany],
                    token.accessToken
                )
                    .enqueue(object : Callback<ArrayList<Point>> {
                        override fun onFailure(call: Call<ArrayList<Point>>, t: Throwable) {
                            points.value = points.value
                        }

                        override fun onResponse(
                            call: Call<ArrayList<Point>>,
                            response: Response<ArrayList<Point>>
                        ) {
                            if (response.isSuccessful) {
                                response.body()?.let { pointList ->
                                    val realm = Realm.getDefaultInstance()
                                    realm.executeTransaction {
                                        it.insertOrUpdate(pointList)
                                    }
                                    points.value = pointList
                                }
                            } else {
                                points.value = points.value
                            }
                        }
                    })

            }

        }

//        fun syncCreate(path: File, token: Token?, user: LoggedInUser) {
//            val realm = Realm.getDefaultInstance()
//            var points = realm.where<Point>()
//                .equalTo("status", "toCreate").findAll()
//
//
//            for (aux in points) {
//                val file = File(path.path + "/project/" + aux.file)
//                create(aux, file, token, user)
//            }
//        }

        fun syncUploadMedia(token: Token?, path: File, user: LoggedInUser) {
            val realm = Realm.getDefaultInstance()
            var points = realm.where<Point>()
                .equalTo("status", "notUploadPhoto").findAll()

            for (aux in points) {
                val file = File(path.path + "/project/" + aux.file)
                uploadFile(aux, file, token, user)
            }
        }
    }

}