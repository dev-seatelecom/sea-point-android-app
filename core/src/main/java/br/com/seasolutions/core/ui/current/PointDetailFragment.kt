package br.com.seasolutions.seapoint.ui.current

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.seasolutions.core.R
import br.com.seasolutions.core.databinding.PointDetailUI
import br.com.seasolutions.seapoint.ui.create.project.CheckCreateFragment
import com.google.android.material.transition.MaterialSharedAxis


/**
 * A simple [Fragment] subclass.
 * Use the [PointDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PointDetailFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val forward = MaterialSharedAxis(MaterialSharedAxis.X, true)
        enterTransition = forward

        val backward = MaterialSharedAxis(MaterialSharedAxis.X, false)
        returnTransition = backward
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = PointDetailUI.inflate(layoutInflater)
        val viewModel = ViewModelProvider(requireActivity())[CurrentProjectViewModel::class.java]

        viewModel.point.observe(this.viewLifecycleOwner, Observer {
            binding.capacity.text = it.capacity.toString()
            binding.height.text = it.height.toString()
            binding.type.text = it.type
            binding.occupantsList.apply {
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
                val list = ArrayList<String>()
                for(aux in it.occupants){
                    list.add(aux.name)
                }
                adapter = Adapter(list)
            }

            binding.accessoryList.apply {
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
                val list = ArrayList<String>()
                for(aux in it.accessories){
                    list.add(aux.name)
                }
                adapter = Adapter(list)
            }

        })


        return binding.root
    }


    class Adapter(private val myDataset: ArrayList<String>) :
        RecyclerView.Adapter<CheckCreateFragment.Adapter.AddViewHolder>() {

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder.
        // Each data item is just a string in this case that is shown in a TextView.

        // Create new views (invoked by the layout manager)
        override fun onCreateViewHolder(parent: ViewGroup,
                                        viewType: Int): CheckCreateFragment.Adapter.AddViewHolder {
            // create a new view
            // set the view's size, margins, paddings and layout parameters
            return CheckCreateFragment.Adapter.AddViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_simple, parent, false))
        }

        // Replace the contents of a view (invoked by the layout manager)
        override fun onBindViewHolder(holder: CheckCreateFragment.Adapter.AddViewHolder, position: Int) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            holder.binding!!.textViewItemSimple.text = myDataset[position]
        }

        // Return the size of your dataset (invoked by the layout manager)
        override fun getItemCount() = myDataset.size
    }


}