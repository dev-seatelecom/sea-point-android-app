package br.com.seasolutions.core.services

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import br.com.seasolutions.seapoint.model.Occupant
import br.com.seasolutions.seapoint.model.Point
import br.com.seasolutions.seapoint.model.Project
import br.com.seasolutions.seapoint.repo.api.project.Repository
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.data.model.Token
import io.realm.Realm
import java.io.File

class UploadService() : Service() {
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        var token: Token = Token().read(applicationContext)
        var loggedInUser: LoggedInUser = LoggedInUser().readUser(applicationContext)
        Thread(Runnable {
            createOccupant(token, loggedInUser)
            createProject(token, loggedInUser)
            createPoint(token, loggedInUser)
            updateProject(token, loggedInUser)
            deleteProject(token, loggedInUser)
            uploadMedia(token, loggedInUser)

        }
        ).start()

        return super.onStartCommand(intent, flags, startId)
    }

    fun createProject(token: Token, loggedInUser: LoggedInUser) {
        var list = Realm.getDefaultInstance().where(Project::class.java)
            .equalTo("status", "toCreate").findAll()

        for (project in list) {
            var count = 0
            do {
                count++
                if (count == 3) {
                    break
                }
            } while (!Repository(token, loggedInUser).create(project))
        }
    }

    fun updateProject(token: Token, loggedInUser: LoggedInUser){
        var list = Realm.getDefaultInstance().where(Project::class.java)
            .equalTo("status", "toUpdate").findAll()


        for (project in list) {
            var count = 0
            do {
                count++
                if (count == 3) {
                    break
                }
            } while (!Repository(token, loggedInUser).update(project))
        }
    }

    fun deleteProject(token: Token, loggedInUser: LoggedInUser) {
        var list = Realm.getDefaultInstance().where(Project::class.java)
            .equalTo("status", "toDelete").findAll()

        for (project in list) {
            var count = 0
            do {
                count++
                if (count == 3) {
                    break
                }
            } while (!Repository(token, loggedInUser).delete(project))
        }
    }

    fun createOccupant(token: Token, loggedInUser: LoggedInUser) {
//        var list2 = Realm.getDefaultInstance().where(Occupant::class.java).findAll()
        var list = Realm.getDefaultInstance().where(Occupant::class.java)
            .equalTo("status", "toCreate").findAll()

        for (occupant in list) {
            var count = 0
            do {
                count++
                if (count == 3) {
                    break
                }
            } while (!br.com.seasolutions.seapoint.repo.api.occupant.Repository(token, loggedInUser)
                    .create(occupant))
        }
    }

    fun createPoint(token: Token, loggedInUser: LoggedInUser) {
        var list = Realm.getDefaultInstance().where(Point::class.java)
            .equalTo("status", "toCreate").findAll()

        for (point in list) {

            var count = 0
            val file =
                File(applicationContext.externalMediaDirs.firstOrNull(), "/project/" + point.file)
            do {
                count++
                if (count == 3) {
                    break
                }
            } while (!br.com.seasolutions.seapoint.repo.api.point.Repository.create(point._id, file, token, loggedInUser))
        }
    }

    fun uploadMedia(token: Token, loggedInUser: LoggedInUser) {
        var count = 0

        val list = Realm.getDefaultInstance().where(Point::class.java)
            .equalTo("status", "notUploadPhoto").findAll()

        for (point in list) {

            val file =
                File(
                    applicationContext.externalMediaDirs.firstOrNull(),
                    "/project/" + point.file
                )
            do {
                count++
                if (count == 3) {
                    break
                }
            } while (!br.com.seasolutions.seapoint.repo.api.point.Repository.uploadFile(point, file, token, loggedInUser))

        }
    }
}