package br.com.seasolutions.seapoint.model

import android.os.Parcel
import com.google.gson.JsonObject
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey
import org.bson.types.ObjectId

open class Occupant(
    var status: String = "toCreate",
    var asn: String = "",
    var scm: String = "",
    var name: String = "",
    @Ignore var isChecked: Boolean = false,
    var companyId: String = "",
    @PrimaryKey var _id: String = ObjectId().toString()
): RealmObject() {
    
    fun getJson(): JsonObject {
        val json = JsonObject()

        json.addProperty("name", name)
        json.addProperty("id", _id.toString())
        json.addProperty("scm", scm)
        json.addProperty("asn", asn)

        return json
    }

    constructor(parcel: Parcel) : this(
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readByte() != 0.toByte(),
        parcel.readString().toString()
    ) {
    }

    class Occupants(): ArrayList<Occupant>(){
        override fun toArray(): Array<String> {
            val list = ArrayList<String>()
            for(i in this){
                list.add(i.toString())
            }
            return list.toTypedArray()
        }

        override fun toString(): String {
            var list = String()
            list+= "["
            for((i,obj) in this.withIndex()){
                if(i<(this.size-1)) list+=(obj.toString()) + ","
                else list+=(obj.toString())
            }
            list+= "]"
            return list
        }
    }
    override fun toString(): String {
        return """
            {
            "id": "$_id",
            "name": "$name",
            "asn": "$asn",
            "scm": "$scm"
            }
        """.trimIndent()
    }

}